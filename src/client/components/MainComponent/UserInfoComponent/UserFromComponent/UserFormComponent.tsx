import { Theme } from "@material-ui/core";
import { InputLabelProps } from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { inject, observer } from "mobx-react";
import React, { Component } from "react";

import { UserInfoStore } from "client/stores";
import { withStyles, WithStyles } from "client/ui";

const styles = (theme: Theme) => ({
    container: {
        display: "flex",
        flexDirection: "column",
        marginRight: "10%",
        width: "40%",
    },
    field: {
        ["&:last-child"]: {
            marginBottom: 0,
        },
        marginBottom: 20,
    },
});

interface IUserFormProps extends WithStyles<typeof styles> {
    userInfoStore?: UserInfoStore;
}

@withStyles(styles)
@inject(UserInfoStore.key)
@observer
export class UserFormComponent extends Component<IUserFormProps> {
    public render() {
        const { classes, userInfoStore } = this.props;
        const labelProps: InputLabelProps = {
            shrink: true,
        };

        return (
            <div className={classes.container}>
                <Typography variant="title">
                    О Вас
                </Typography>
                <TextField
                    id="name"
                    label="Имя"
                    autoComplete="off"
                    className={classes.field}
                    placeholder="Иванов Иван Иванович"
                    InputLabelProps={labelProps}
                    margin="normal"
                    value={userInfoStore.userInfo.name}
                    onChange={this.onNameChange}
                />
                <TextField
                    id="phone"
                    label="Телефон"
                    autoComplete="off"
                    className={classes.field}
                    placeholder="+7 000 000 00 00"
                    InputLabelProps={labelProps}
                    margin="normal"
                    value={userInfoStore.userInfo.phone}
                    onChange={this.onPhoneChange}
                />
                <TextField
                    id="email"
                    label="E-mail"
                    autoComplete="off"
                    className={classes.field}
                    placeholder="yourmail@gmail.com"
                    InputLabelProps={labelProps}
                    margin="normal"
                    value={userInfoStore.userInfo.email}
                    onChange={this.onEmailChange}
                />
            </div>
        );
    }

    private onNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.userInfoStore.userInfo.setName(e.target.value);
    }

    private onPhoneChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.userInfoStore.userInfo.setPhone(e.target.value);
    }

    private onEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.userInfoStore.userInfo.setEmail(e.target.value);
    }
}
