import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import classNames from "classnames";
import React, { Component } from "react";

import { SelectableOption } from "client/models";
import { RadioButtonIcon, withStyles, WithStyles } from "client/ui";

const styles = () => ({
    caption: {
        color: "#bbbbbb",
        fontSize: 15,
    },
    label: {
        color: "#161616",
        marginBottom: 20,
        textTransform: "uppercase",
    },
    option: {
        alignItems: "flex-start",
        width: "50%",
    },
    radio: RadioButtonIcon.RadioButtonIconStyle,
    radioClass: {
        marginRight: 14,
    },
    row: {
        display: "flex",
        flexDirection: "row",
    },
});

interface IPowerswitchesOptionProps extends WithStyles<typeof styles> {
    options: SelectableOption[];
    selected: string[];
    onSelect: (val: string) => void;
}

@withStyles(styles)
export class PowerswitchesOptionComponent extends Component<IPowerswitchesOptionProps> {
    public render() {
        const { classes, options } = this.props;

        return (
            <div>
                <Typography variant="title">Розетки и выключатели</Typography>
                <div className={classes.row}>
                    {options.map(this.renderOption)}
                </div>
            </div>
        );
    }

    private renderOption = (option: SelectableOption) => {
        const { classes, selected } = this.props;
        return (
            <div key={option.value} className={classNames(classes.row, classes.option)}>
                <Checkbox
                    value={option.value}
                    className={classes.radioClass}
                    checked={selected.indexOf(option.value) >= 0}
                    classes={{ root: classes.radio }}
                    onChange={this.onOptionSelect}
                    icon={<RadioButtonIcon checked={false} />}
                    checkedIcon={<RadioButtonIcon checked={true} />}
                />
                <div>
                    <Typography className={classes.label}>{option.label}</Typography>
                    <Typography className={classes.caption} variant="caption">
                        {option.description}
                    </Typography>
                </div>
            </div>
        );
    }

    private onOptionSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.onSelect(e.target.value);
    }
}
