import { action, computed, observable } from "mobx";
import { DoorOption } from "./DoorOption";
import { FloorOption } from "./FloorOption";
import { Option } from "./Option";
import { SelectableOption } from "./SelectableOption";

export enum CapsuleType {
    anonimous,
    amnezia,
    marshmallow,
    transformer,
}

export enum OptionTypes {
    wall = "Стены",
    floor = "Напольные покрытия",
    ceil = "Потолок",
    door = "Двери",
    baseboard = "Плинтус и карниз",
    power = "Розетки и выключатели",
    summary = "Итого",
    additional = "Дополнительные опции",
    package = "«комплектация»",
}

export interface IOption {
    type: OptionTypes;
    options: Option[] | SelectableOption[] | FloorOption[] | DoorOption[];
}

export interface ISelectedOption {
    type: OptionTypes;
    values: string[];
}

export class Capsule {
    private _title: string;
    private _type: CapsuleType;
    private _description: string;
    private _image: string;
    private _link: string;
    private _options: IOption[];

    @observable
    private _selectedOptions: ISelectedOption[];

    constructor(
            title: string,
            type: CapsuleType,
            description: string,
            image: string,
            link: string,
            options: IOption[],
    ) {
        this._title = title;
        this._type = type;
        this._description = description;
        this._image = image;
        this._link = link;
        this._options = options;
        this._selectedOptions = options.map((opt) => {
            let values: string[];

            switch (opt.type) {
                case OptionTypes.additional:
                case OptionTypes.package:
                    values = [];
                    break;
                default:
                    values = opt.options[0] ? [opt.options[0].value] : [];
            }

            return {
                type: opt.type,
                values,
            };
        });
    }

    public get title() {
        return this._title;
    }

    public get type() {
        return this._type;
    }

    public get image() {
        return this._image;
    }

    public get description() {
        return this._description;
    }

    public get link() {
        return this._link;
    }

    public get options() {
        return this._options;
    }

    public getSelectedOption(type: OptionTypes) {
        const tmp = this._selectedOptions.filter((opt) => opt.type === type) || [];
        return tmp[0];
    }

    @action
    public toggleSelected(type: OptionTypes, value: string) {
        const selectedOptions: ISelectedOption[] =
            this._selectedOptions.filter((opt) => opt.type === type)
            || [{ type, values: [] }];

        const option = selectedOptions[0];
        const contains = option.values.indexOf(value) >= 0;

        if (type === OptionTypes.summary) {
            option.values = [value];
        } else if (contains) {
            option.values = option.values.filter((v) => v !== value);
        } else {
            option.values.push(value);
        }

        this._selectedOptions = [
            ...this._selectedOptions.filter((opt) => opt.type !== type),
            option,
        ];
    }

    public toString() {
        let str: string;
        str = `Капсула ${this._title} \n`;
        str = str + this._selectedOptions.map((opt) => {
            const fullOpt = this._options.filter((o) => o.type === opt.type)[0];
            const fullVars = (fullOpt.options as Option[]).filter((o) => opt.values.indexOf(o.value) > -1);
            return fullOpt.type + ": " +
                (fullVars && fullVars.length > 0 ? fullVars.map((v) => v.toString()).join(", ") : "Пусто");
        }).join("\n");

        return str;
    }
}
